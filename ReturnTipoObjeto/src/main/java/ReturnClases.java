public class ReturnClases {
    public static void main(String[] args) {
        // crea el objeto indirectamente con el metodo
        Suma s = creaObjetoSuma();
        
        //objeto con los metodos
        System.out.println("Resultado metodo sumar: "+ s.sumar());
    }
    
    //metodo
    private static Suma creaObjetoSuma(){
        Suma suma = new Suma(3,2);
        return suma;
    }
}

class Suma {
    //atributos
    int a;
    int b;
    
    //constructor
    public Suma(int a, int b){
        this.a = a;
        this.b = b;
    }
    
    //metodo
    public int sumar(){
        return this.a + this.b;
    }
}