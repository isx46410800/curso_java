package ventas;

import gm.com.ventas.*;

public class Ventas {
    public static void main(String[] args) {
        //crear varios objetos de tipo producto
        Producto producto1 = new Producto("Camisa",50);
        Producto producto2 = new Producto("Pantalon",100);
        
        // crear objetos de tipo orden
        Orden orden1 = new Orden();
        //agregamos los productos a la orden
        orden1.agregarProducto(producto1);
        orden1.agregarProducto(producto2);
        
        //imprimir esta orden
        orden1.mostrarOrden();
    }
}
