package gm.com.ventas;

public class Orden {
    //atributos
    private int idOrden;
    private Producto productos[];//es un arreglo referencia a producto
    private static int contadorOrdenes;
    private int contadorProductos;
    private static final int MAX_PRODUCTOS = 10;//constante sin poder modificar
    
    // constructor vacio
    public Orden(){
        this.idOrden = ++contadorOrdenes;
        //inicializamos el arreglo
        productos = new Producto[MAX_PRODUCTOS];
    }
    // metodo agregamos producto
    public void agregarProducto(Producto producto){
        if (this.contadorProductos < MAX_PRODUCTOS) {
            productos[this.contadorProductos++] = producto;
        } else {
            System.out.println("Se ha superado el maximo de productos: "+ MAX_PRODUCTOS);
        }
    }
    // metodo calcular total
    public double calcularTotal(){
        double total = 0;
        for(int i=0; i < this.contadorProductos;i++){
            Producto producto = this.productos[i];
            total += producto.getPrecio();
        }
        return total;
    }
    //mostrar orden
    public void mostrarOrden(){
        System.out.println("Orden: "+ this.idOrden);
        double totalOrden = this.calcularTotal();
        System.out.println("Total de la orden: "+ totalOrden);
        System.out.println("Productos de la orden: ");
        for(int i=0;i < this.contadorProductos++;i++){
            Producto producto = productos[i];
            System.out.println(productos[i]);
        }
    }
}
