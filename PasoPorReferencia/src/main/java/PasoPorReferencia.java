public class PasoPorReferencia {
    public static void main(String[] args) {
        // objeto
        Persona persona = new Persona();
        
        //usamos los metodos para cambiar el nombre
        persona.cambiarNombre("Miguel");
        System.out.println("Nombre: "+ persona.obtenerNombre());
        
        //metodo
        modificarPersona(persona);
        
        System.out.println("Nombre: "+ persona.obtenerNombre());

    }
    //metodo modificarPersona (cambiamos nombre de arg)
    private static void modificarPersona(Persona personaArg) {
        personaArg.cambiarNombre("Miguelito");
    }
}
