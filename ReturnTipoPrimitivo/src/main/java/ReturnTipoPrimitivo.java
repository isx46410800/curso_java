public class ReturnTipoPrimitivo {
    public static void main(String[] args) {
        sumarSinRetornarValor(3,9);
        int resultado = sumarRetornarValor(5,5);
        System.out.println("Return retornar valor: "+ resultado);
    }

    private static void sumarSinRetornarValor(int a, int b) {
        System.out.println("Return sin retornar valor: "+ (a+b));
        //return; es opcional, si es tipo void no retorna nada
    }

    private static int sumarRetornarValor(int a, int b) {
        if (a==0 && b==0){
            return 0; //si llega a este return no llega al otro.
        }
        return a+b; //obligatorio al poner tipo int y no void
    }
}
