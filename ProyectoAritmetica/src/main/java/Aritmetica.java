public class Aritmetica {
    // atributos de la clase. Por defecto es 0
    int a;
    int b;
    
    //definimos un constructor vacio y se llama como la clase
    public Aritmetica(){
        System.out.println("Ejecutando constructor vacio");
    }
    
    //definimos un constructor de tipo publico con argumentos
    public Aritmetica(int arg1, int arg2){
        a = arg1;
        b = arg2;
        System.out.println("Ejecutando constuctor con argumentos");
    }
    
    // creacion de un metodo
    // si ponemos argumentos, no son las variables de los atributos
    public int sumar(){
        //cuerpo de creacion de nuestro metodo
        return a + b; //el valor debe ser mismo tipo que sintaxi de public    
    }
    
    public int restar(){
        return a - b;
    }
    
    public int multiplicar(){
        return a * b;
    }
    
    public int dividir(){
        return a / b;
    }
}
