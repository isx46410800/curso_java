
public class PruebaAritmetica {

    // si se crea aqui las variables se puede usar en todos los metodos de la clase
    public static void main(String[] args) {

        // variables local solo para este metodo
        int operandoA = 6;
        int operandoB = 2;

        // creamos objeto
        Aritmetica objeto1 = new Aritmetica(operandoA, operandoB);

        //resultado
        System.out.println("Operando A: " + operandoA);
        System.out.println("Operando B: " + operandoB);
        
        //resultado metodo suma
        System.out.println("Resultado suma: " + objeto1.sumar());
        System.out.println("Resultado multi: " + objeto1.multiplicar());

    }

    //metodo
    //public static void otroMetodo() {
    //  System.out.println("");
}
}
