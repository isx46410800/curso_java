
public class PasoPorValor {

    public static void main(String[] args) {
        // valor primitivo
        var x = 10;
        System.out.println("x = " + x);

        // metodo
        // vemos que se le pasa la copia pero no cambia el valor de x despues
        cambiarValor(x); //click a la alerta y se crea el metodo debajo

        //vuelve a tener x su valor
        System.out.println("x = " + x);
    }

    //privado significa que solo se puede usar en esta clase
    private static void cambiarValor(int arg) {
        arg = 20;
        System.out.println("x = " + arg);
    }
}