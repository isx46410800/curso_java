
public class PruebaPersonas {

    public static void main(String[] args) {
        // creacion de un objeto de tipo persona

        // definimos la variable de tipo Persona en ref class Persona
        Persona persona1;

        // creando un objeto de la clase Persona
        persona1 = new Persona();

        // accedemos al objeto y llamamos a los metodos y atributos de clase Persona
        persona1.desplegarNombre();
        
        // asignamos valores de los atributos del objeto Persona
        persona1.nombre = "Miguel";
        persona1.apellido = "Amoros";
        persona1.desplegarNombre();
        
        // creamos otro objeto
        Persona persona2 = new Persona();
        persona2.nombre = "Natalia";
        persona2.apellido = "Sendra";
        persona2.desplegarNombre();
    }
}
