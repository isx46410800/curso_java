public class Persona {
    //atributos
    private String nombre;
    private double sueldo;
    private boolean eliminado;
    
    public Persona(String nombre, double sueldo, boolean eliminado){
        this.nombre = nombre;
        this.sueldo = sueldo;
        this.eliminado = eliminado;
    }
    //metodo get para retornar solo en nombre en privado y este metodo será llamado desde otros
    public String getNombre(){
        return this.nombre;
    }
    //metodo publico de la variable private nombre
    public void setNombre(String nombre){
        this.nombre = nombre;
    }
    // al poner privado la variable se puede insertar automaticamente set/get
    public double getSueldo() {
        return sueldo;
    }

    public void setSueldo(double sueldo) {
        this.sueldo = sueldo;
    }

    public boolean isEliminado() {
        return eliminado;
    }

    public void setEliminado(boolean eliminado) {
        this.eliminado = eliminado;
    }
    // para no imprimir tantas veces la linea de print para cada variable
    //metodo DOSTRING
    public String toString(){
        return "Nombre: "+ nombre+ " " + "Sueldo: " + sueldo + " "+ "Eliminado: "+ eliminado;   
    }
            
    
}
