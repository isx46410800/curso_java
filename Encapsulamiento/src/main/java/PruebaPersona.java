public class PruebaPersona {
    public static void main(String[] args) {
        Persona persona = new Persona("Miguel",5000, false);
        // cuando la variable es publica
        //System.out.println("Nombre: "+ persona.nombre);
        // cuando la variable es privada de otra clase, se llama al metodo publico GET/SET
        System.out.println("Nombre: "+ persona.getNombre());
        //llamamos al metodo set de moficiar nombre
        persona.setNombre("MIguelito");
        System.out.println("Nombre: "+ persona.getNombre());
        System.out.println("Sueldo: "+ persona.getSueldo());
        System.out.println("Eliminado?: "+ persona.isEliminado());
        persona.setSueldo(2000);
        persona.setEliminado(true);
        System.out.println("Sueldo: "+ persona.getSueldo());
        System.out.println("Eliminado?: "+ persona.isEliminado());
        // IMPRIMIR TODO EN UNO CON METODO TOSTRING
        System.out.println("persona: "+ persona.toString());
        System.out.println("persona: "+ persona);//default es tostring
    }
}
