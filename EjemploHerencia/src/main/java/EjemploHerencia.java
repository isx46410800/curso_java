
import java.util.Date;


public class EjemploHerencia {

    public static void main(String[] args) {
        //creamos objeto
        Empleado empleado1 = new Empleado("Miguel", 2000);
        //podemos llamarlo porque hereda del padre este atributo
        empleado1.setEdad(28);
        empleado1.setDireccion("Agudells 66");
        System.out.println(empleado1);
        
        //creamos objeto
        Cliente cliente1 = new Cliente(new Date(),true);
        System.out.println(cliente1);
    }
}
