public class Empleado extends Persona {
    //al poner extends hereda todos las class public de Persona
    //atributos
    private int idEmpleado;
    private double sueldo;
    private static int contadorEmpleado;
    
    //constructor
    //se utiliza super para llamar a un atributo de la clase padre
    //ha de ser siempre la primera linea SUPER
    public Empleado(String nombre,double sueldo){
        super(nombre);
        this.idEmpleado = ++contadorEmpleado;
        this.sueldo = sueldo;
    }
    //metodos get /set

    public int getIdEmpleado() {
        return idEmpleado;
    }

    public double getSueldo() {
        return sueldo;
    }

    public void setSueldo(double sueldo) {
        this.sueldo = sueldo;
    }

    public static int getContadorEmpleado() {
        return contadorEmpleado;
    }

    @Override
    public String toString() {
        return super.toString() + "Empleado{" + "idEmpleado=" + idEmpleado + ", sueldo=" + sueldo + '}';
    }
    
    
}
