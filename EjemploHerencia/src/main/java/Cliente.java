
import java.util.Date;

public class Cliente extends Persona {
    //al poner extends hereda todos las class public de Persona
    //atributos
    private int idCliente;
    private Date fechaRegistro;
    private boolean vip;
    private static int contadorClientes;
    //constructor
    //se utiliza super para llamar a un atributo de la clase padre
    public Cliente(Date fechaRegistro, boolean vip){
        this.idCliente = idCliente;
        this.idCliente = ++contadorClientes;
        this.vip = vip;
    }
    //metodos get /set

    public int getIdCliente() {
        return idCliente;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public boolean isVip() {
        return vip;
    }

    public void setVip(boolean vip) {
        this.vip = vip;
    }

    public static int getContadorClientes() {
        return contadorClientes;
    }
    //metodo to string
    @Override
    public String toString() {
        return super.toString() + "Cliente{" + "idCliente=" + idCliente + ", fechaRegistro=" + fechaRegistro + ", vip=" + vip + '}';
    }
    
    
}
