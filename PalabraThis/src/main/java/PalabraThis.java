public class PalabraThis {
    //metodo
    public static void main(String[] args) {
        Persona persona = new Persona("Miguel");
    }
}

class Persona{
    String nombre;
    
    //constructor
    Persona(String nombre){
        this.nombre = nombre;//this apunta a un objeto tipo persona
        // creamos objeto
        Imprimir imprimir = new Imprimir();
        imprimir.imprimir(this);//contiene ref al objeto persona
        
    }
}

class Imprimir{
    //metodo
    public void imprimir(Persona p1) {
        //valor del objeto persona
        System.out.println("Impresion argumento persona; " + p1);
        // this apunta a un objeto de tipo imprimir
        System.out.println("Impresion objeto actual(this); " + this);
    }
}