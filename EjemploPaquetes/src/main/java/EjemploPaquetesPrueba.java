//importamos con.gm.utileria o todas con com.gm.*
import com.gm.Utileria;
//import com.gm.*;
//import static com.gm.utileria.imprimir

public class EjemploPaquetesPrueba {
    public static void main(String[] args) {
        //importamos paquete con el nombre de la clase completo
        com.gm.Utileria.imprimir("Hola Miguel!");
        
        //import de la clase(se agrega automaticamente arriba)
        Utileria.imprimir("Holaaaa");
        //imprimit("holaaa); si importamos `import static com.gm.utileria.imprimir
    }
}
