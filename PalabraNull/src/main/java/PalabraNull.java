public class PalabraNull {
    public static void main(String[] args) {
        // crear objeto y llamar al metodo de la clase Persona
        Persona persona1 = new Persona("Miguel");
        System.out.println("Nombre: "+ persona1.ObtenerNombre());
        
        Persona persona2 = persona1;
        System.out.println("Nombre: "+ persona2.ObtenerNombre());
        
        persona1 = null;
        if (persona1!=null){
            System.out.println("Nombre: "+ persona1.ObtenerNombre());
        } else {
            System.out.println("Nombre: nulo");
        }
    }
}

class Persona{
    String nombre;
    //constructor
    Persona(String nombre){
        this.nombre = nombre;
    }
    //metodo
    public String ObtenerNombre(){
        return this.nombre;
    }
}
